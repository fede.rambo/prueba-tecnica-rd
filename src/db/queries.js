const connection = require('./connect');


const insertData = async ({ field_1,author,description,my_numeric_field }) => {
    try{
        const result = await connection.query(
            `INSERT INTO tabla VALUES(null, ?, ?, ?, ?)`,[field_1,author,description,my_numeric_field]
        ); 
        return result;   
    }
    catch(error){
        console.error('database upload failed', error)
        throw error;
    }
}

const getData = async (id) => {
    try{
        const result = await connection.query(
            `SELECT * FROM tabla WHERE id = ?`,id
        );
        return result;
    }
    catch(error){
        console.error('failed to obtain registry from database', error)
        throw error;
    }
}

module.exports = {
    insertData,
    getData
}
 