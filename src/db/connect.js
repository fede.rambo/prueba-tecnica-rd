const mysql = require('mysql2');
const util = require('util');

const connectionConfig = {
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    port: process.env.DB_PORT
    }

const connection = mysql.createConnection(connectionConfig);
connection.query = util.promisify(connection.query);


connection.connect((err) => {
    if (err){
        console.error(err)
        console.error('error while trying to connect to database');
    }
    else{
        console.log('successfully connected to database');
    }
})

module.exports = connection;
