const queries = require('../../db/queries');


const postData = async (req,res) =>{
    try{
    const campo = req.params.field
    req.body[campo] = req.body[campo].toUpperCase();
    const result = await queries.insertData(req.body)
    const id = result.insertId
    return res.status(200).send({
        id
    })
    }
    catch(error){
        console.error(error)
        return res.status(400).send({
            success: false,
            message: 'something went wrong'
        });
    }   
}

const getData = async (req,res) =>{
    try{
        const id = req.params.id;
    const result = await queries.getData(id);
    return res.status(200).send(result[0]);  
    }
    catch(error){
        console.error(error);
        return res.status(400).send({
            success: false,
            message: 'something went wrong'
        });
    }
}

module.exports = {
    postData,
    getData
}