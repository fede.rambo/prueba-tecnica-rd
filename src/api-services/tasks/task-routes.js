const express = require('express');
const { postData,getData } = require('./task-controller');
const router = express.Router();
const validUrl = require('../../middlewares/valid-url');


router
  .route('/input/:field')
  .post(
    validUrl,
    postData   
  )

router
  .route('/get_data/:id')
  .get(
    getData
  )

module.exports = router
