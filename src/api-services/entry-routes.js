const express = require('express');
const taskRoutes = require('./tasks/task-routes');

const router = express.Router();

router.use('/', taskRoutes);

module.exports = router