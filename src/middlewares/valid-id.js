const dbmock = require('../dbmock.json')
const validId = (req,res,next) =>{
    const id = req.params.id;
    for(let i=0;i<dbmock.length;i++){
        if(id == dbmock[i].id){
            return next();
        }
    }
    return res.status(400).send({
        error: `${id} no es una ID válida.`
    })  
}

module.exports = validId
