const { json: expressJson } = require('express');
const entryRoutes = require('../api-services/entry-routes')


const applyMiddlewares = (app) => {
    app.use(expressJson());
    app.use(entryRoutes);
}

module.exports = applyMiddlewares;