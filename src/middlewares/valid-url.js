const validUrl = (req,res,next) =>{
    const campo = req.params.field;
    if (campo != 'author' && campo != 'field_1' && campo != 'description'){
        return res.status(400).send({
            error: `${campo} no es un campo válido para convertir a mayúscula.`,
        })
    }
    return next()
}

module.exports = validUrl
