const express = require('express');
const applyMiddlewares = require('./src/middlewares/applymiddlewares');
require('./src/db/connect');

const app = express();

const port = process.env.PORT || 3000;
const server = app.listen(port, () => console.log(`Server hosted in port ${port}`));


applyMiddlewares(app);

