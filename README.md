#   REPUTACION-DIGITAL-PRUEBA-TECNICA
Technical evaluation for backend developers.

## Installation

Use the node package manager (npm) to install its dependencies

```bash
npm install
```

## Usage

To initialize the project, run
```npm
npm start
```

## Hosting

This app is hosted via Heroku. Its URL is the following:

https://rd-backend-test.herokuapp.com/

Requests can be handled via postman, a request collection is included in the main branch so both POST and GET requests can be executed with their respective endpoints (/input/ for POST and /get_data/ for GET).

Example for GET: https://rd-backend-test.herokuapp.com/get_data/1
